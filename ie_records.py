import sys
import pyorient
import os
import json
import zipfile

ip = os.getenv('COREVISION_SERVER', "localhost")

db_name = "corevision"

orient_user = "root"
orient_pass = "root"

db_user = "admin"
db_pass = "admin"

images_path = "../images/"

# read config from json-file
if os.path.exists("config.json"):
    with open("config.json", "r") as fl:
        conf = json.loads(fl.read())

        if 'ip' in conf:
            ip = conf['ip']

client = pyorient.OrientDB(ip, 2424)

try:
    client.connect(orient_user, orient_pass)
    clusters = client.db_open(db_name, db_user, db_pass, pyorient.DB_TYPE_GRAPH, "")
    print("Database: Connected")
except pyorient.exceptions.PyOrientConnectionException:
    print("Database: Can't  connect...")
except:
    e = sys.exc_info()
    print(e)


unknownBranch = client.query("SELECT * FROM Branch WHERE name='Unknown'", 10000)
if len(unknownBranch) > 0:
    unknownBranch = {"link": pyorient.otypes.OrientRecordLink(unknownBranch[0]._rid[1:]), "record": unknownBranch[0]}
else:
    new_branch = {
        '@BranchEvent': {
            "name": "Unknown",
        }
    }

    bid = client.record_create(client.get_class_position(b'branch'), new_branch)
    unknownBranch = {"link": pyorient.otypes.OrientRecordLink(bid._rid[1:]), "record": bid}


def prepareRecord(item):
    data = item.oRecordData
    for i in data:
        field = data[i]

        if type(field) == pyorient.otypes.OrientRecordLink:
            rec_id = field.clusterID + ":" + field.recordPosition
            data[i] = rec_id
    return data

users_raw = client.query("SELECT FROM User", 10000)
users = {}
for user in users_raw:
    name = user.firstName + " " + user.lastName
    users[name] = {"record": user, "link": pyorient.otypes.OrientRecordLink(user._rid[1:])}


def getUserByName(firstName, lastName):
    name = firstName + " " + lastName
    if name in users:
        return users[name]

    return None

def createUser(firstName, lastName, branch, image = {"link": None}):
    branchLink = pyorient.otypes.OrientRecordLink(branch) if branch is not None else unknownBranch['link']

    new_user = {
        '@UserEvent': {
            "firstName": firstName,
            "lastName": lastName,
            "image": image['link'],
            "branch": branchLink,
            "recognitionLevel": 0
        }
    }

    uid = client.record_create(client.get_class_position(b'user'), new_user)
    name = firstName + " " + lastName
    print("User %s was created" % name)
    users[name] = {"link": pyorient.otypes.OrientRecordLink(uid._rid[1:]), "record": uid}
    return users[name]

def isSessionExist(session):
    sessions = client.query("SELECT * FROM Session WHERE user.firstName = '{userFirstName}' AND user.lastName = '{userLastName}' AND dateTime = {dateTime}"
                            .format(**session['record']), 10000)

    if len(sessions) > 0:
        return sessions[0]._rid[1:]

    return None


images_raw = client.query("SELECT FROM Image", 1000000)
images = {}
for image in images_raw:
    name = image.name + ":" + image.path
    images[name] = {"record": image, "link": pyorient.otypes.OrientRecordLink(image._rid[1:])}


def getImageByName(name, path):
    name = name + ":" + path
    if name in images:
        return images[name]

    return None


def createImage(name, path):
    new_image = {
        '@ImageEvent': {
            "name": name,
            "path": path
        }
    }

    iid = client.record_create(client.get_class_position(b'image'), new_image)
    print("Image %s was created" % path)

    images[path] = {"link": pyorient.otypes.OrientRecordLink(iid._rid[1:]), "record": iid}
    return images[path]


def isEmbeddingExist(embedding):
    record = client.record_load(embedding['rid'])
    if record._class is not None and record.dateTime == embedding['dateTime']:
        return record._rid[1:]

    return None


def exportData(path):
    if not os.path.exists(path):
        os.makedirs(path)
    if not path.endswith('/'):
        path += '/'

    print("Start export data to " + path)

    sessions = client.query(sessionsSQL, 100000)
    sessions = list(map(prepareRecord, sessions))
    sessions_json = json.dumps(sessions)
    file = open(path + "sessions.json", "w")
    file.write(sessions_json)
    file.close()

    embeddings = client.query(embeddingsSQL, 100000)
    embeddings = list(map(prepareRecord, embeddings))
    embeddings_json = json.dumps(embeddings)
    file = open(path + "embeddings.json", "w")
    file.write(embeddings_json)
    file.close()

    images = client.query(imagesSQL, 100000)
    images = list(map(prepareRecord, images))
    # images_json = json.dumps(images)
    # file = open(path + "images.json", "w")
    # file.write(images_json)
    # file.close()
    with zipfile.ZipFile(os.path.abspath(path + "images.zip"), 'w') as zipFile:
        for image in images:
            zipFile.write(images_path + image['path'], compress_type=zipfile.ZIP_DEFLATED)


def importData(path):
    if not path.endswith('/'):
        path += '/'

    print("Start import data from " + path)

    file = open(path + "sessions.json", "r")
    sessions_json = file.read()
    file.close()
    sessions_raw = json.loads(sessions_json)
    sessions = {}
    for session in sessions_raw:
        sessions[session['rid']] = {"record": session, "link": pyorient.otypes.OrientRecordLink(session['rid']), "isNew": False}

    file = open(path + "embeddings.json", "r")
    embeddings_json = file.read()
    file.close()
    embeddings = json.loads(embeddings_json)

    with zipfile.ZipFile(os.path.abspath(path + "images.zip"), 'r') as zipFile:
        zipFile.extractall(os.path.abspath("../"))

    for i in sessions:
        session = sessions[i]
        if 'userFirstName' not in session['record'] or 'userLastName' not in session['record']:
            continue

        sid = isSessionExist(session)
        if sid is None:
            image = getImageByName(session['record']['imageName'], session['record']['imagePath'])
            if image is None:
                image = createImage(session['record']['imageName'], session['record']['imagePath'])
            user = getUserByName(session['record']['userFirstName'], session['record']['userLastName'])
            if user is None:
                user = createUser(session['record']['userFirstName'], session['record']['userLastName'], session['record'].get('userBranch'), image)
            new_session = {
                '@SessionEvent': {
                    "dateTime": session['record']['dateTime'],
                    "source": session['record']['source'] if 'source' in session['record'] else None,
                    "user": user['link'],
                    "image": image['link'],
                    "isConfirmed": session['record'].get('isConfirmed'),
                    "isNormalized": session['record'].get('isNormalized')
                }
            }

            sid = client.record_create(client.get_class_position(b'session'), new_session)
            print("Session %s was created" % sid._rid)
            session['link'] = pyorient.otypes.OrientRecordLink(sid._rid[1:])
            session['isNew'] = True

    for embedding in embeddings:
        if embedding['session'] in sessions:
            session = sessions[embedding['session']]
            if session['isNew']:
                eid = isEmbeddingExist(embedding)
                if eid is None:
                    image = getImageByName(embedding['imageName'], embedding['imagePath'])
                    if image is None:
                        image = createImage(embedding['imageName'], embedding['imagePath'])
                    new_embedding = {
                        '@EmbeddingEvent': {
                            "dateTime": embedding['dateTime'],
                            "image": image['link'],
                            "session": session['link'],
                            "embedding": embedding['embedding']
                        }
                    }

                    eid = client.record_create(client.get_class_position(b'embedding'), new_embedding)
                    print("Embedding %s was created" % eid._rid)




sessionsWhereSQL = "FROM Session WHERE source NOT IN ['data.json', 'average']"
sessionsSQL = "SELECT @rid, dateTime, isConfirmed, isNormalized, source, image.name AS imageName, image.path AS imagePath, user.branch AS userBranch, user.firstName AS userFirstName, user.lastName AS userLastName %s" % sessionsWhereSQL
embeddingsSQL = "SELECT @rid, dateTime, embedding, image.name AS imageName, image.path AS imagePath, session.@rid.asString().substring(1) AS session " \
                "FROM embedding WHERE session in (SELECT * %s)" % sessionsWhereSQL
imagesSQL = "SELECT image.@rid AS rid, image.name AS name, image.path AS path FROM embedding WHERE session in (SELECT * %s)" % sessionsWhereSQL


# exportData("../dump/vinnytsia2")
importData("../dump/webcam")
