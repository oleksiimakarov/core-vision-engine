from http.server import HTTPServer, BaseHTTPRequestHandler
import sys
import os
import cgi

import json

image_server_ip = "127.0.0.1"

# read config from json-file
if os.path.exists("config.json"):
    with open("config.json", "r") as fl:
        conf = json.loads(fl.read())

        if 'image_server_ip' in conf:
            image_server_ip = conf['image_server_ip']


class CorevisionHTTPRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        print(self.path)

        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_POST(self):
        post_vars = {}
        ct_header = self.headers['Content-Type']
        content_length = int(self.headers['Content-Length'])
        print("content length: " + str(content_length))
        if ct_header == 'application/x-www-form-urlencoded':
            post_vars = cgi.parse_qs(self.rfile.read(content_length), keep_blank_values=1)
        else:
            print(self.path)
            body = self.rfile.read(content_length)

        if self.path[:4] == '/api':
            parts = self.path.split("/")
            print(len(parts))

            try:
                if self.path[:11] == '/api/delete':
                    # user_id = parts[3].replace('-', ':')
                    image_name = parts[4]
                    file_name = '../images/' + image_name + '.jpg'
                    if os.path.exists(file_name):
                        os.remove(file_name)
                    else:
                        self.file_not_found()
                        return
                    self.send_response(200)
                    self.end_headers()
                else:
                    # user_id = parts[2].replace('-', ':')
                    image_name = parts[3]
                    file_name = '../images/' + image_name + '.jpg'
                    try:
                        with open(file_name, 'wb') as f:
                            f.write(body)
                    except:
                        self.file_not_found()
                        return
                    self.send_response(200)
                    self.end_headers()
            except:
                e = sys.exc_info()[0]
                print(e)
                self.file_not_found()
                return
        else:
            self.file_not_found()
            return


    def file_not_found(self):
        self.send_response(404)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

httpd = HTTPServer((image_server_ip, 7777), CorevisionHTTPRequestHandler)

print("Data server is listening on port 7777")

httpd.serve_forever()
