sudo apt-get update && apt-get install -y \
      build-essential \
      curl \
      wget \
      g++ \
      git \
      iputils-ping \
      libcairo2-dev \
      libjpeg8-dev \
      libpango1.0-dev \
      libgif-dev \
      python2.7 \
      python3.6 \
      python3.6-dev \
      python3-venv \
      sudo \
      tzdata \
      vim \
  && rm -rf /var/lib/apt/lists/* &&


sudo python3 -m venv corevision && . corevision/bin/activate &&
sudo pip3 install -r ../requirements.txt &&


sh install-facenet.sh &&

PYTHONPATH="~/facenet/src:~/core-vision-engine" &&
export PYTHONPATH
