cd ~ &&
git clone https://github.com/davidsandberg/facenet.git ~/facenet &&
mkdir model &&
cd ~/model &&
curl --silent --location --output ./model.tar.bz2.tmp https://github.com/zixia/node-facenet/releases/download/v0.1.9/model-20170512.tar.bz2 &&
mv model.tar.bz2.tmp model.tar.bz2 &&
tar jxvf model.tar.bz2 &&
rm -f model.tar.bz2 &&
