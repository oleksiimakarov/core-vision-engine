## Core Vision Engine

### Database Installation

1. Download [OrientDB 2](https://orientdb.com/download-2/)
2. Unpack the archive, rename folder and place it near "core-vision-engine" project as following:

        some-common-folder/
          |_ orientdb/
          |_ core-vision-engine/

3. Go to _**/cmd**_  directory in core-vision-engine project
4. Run **reset-db** file to create or reset database.
5. Run **start-db** file to start OrientDB server.
