from PIL import Image
import time
from threading import Thread
from multiprocessing import Queue
from http.server import HTTPServer, BaseHTTPRequestHandler
import os
from io import BytesIO
import json
import sys
import hashlib
import base64
import cv2
import numpy as np
import requests
# import scipy
# from io import StringIO
from scipy.spatial import distance
from scipy.spatial.kdtree import KDTree
from corevision.nn_bridge import MtcnnBridge
from corevision.nn_bridge import FacenetBridge
from corevision.database import DataBase, DataBaseSettings
from corevision.embedding_object import NearDistanceObject, EmbeddingObject, SessionObject

# Those configurations use for docker container. Those have to be rewritten in config.json if it needs
HTTP_SERVER_IP = os.getenv('HTTP_SERVER_IP', "127.0.0.1")  # IP which will be listened
ROOT_PATH = os.getenv('COREVISION_WEBCAM', "core-vision-webcam")  # Directory for static files on the same level as core-vision-engine
model_path = os.getenv('FACENET_MODEL', "../model/")  # Directory for files of facenet model
ip = os.getenv('COREVISION_SERVER', "localhost")  # IP of orientDB database
image_server_ip = os.getenv('COREVISION_IMAGE_SERVER', ip)  # IP of data_server.py which receive images

# inputs
images_queue = Queue(2)
faces_queue = Queue(2)
embeddings_queue = Queue(2)
selected_users_queue = Queue(2)

# accumulation
accumulation_queue = Queue(5)

# accumulation
stored_image_queue = Queue(5)

# outputs
responses_users_queue = Queue(2)
responses_embedding_queue = Queue(2)

db_name = "corevision"

orient_user = "root"
orient_pass = "root"

db_user = "admin"
db_pass = "admin"

recreateKDTreeInterval = 60 * 60

# read config from json-file
if os.path.exists("config.json"):
    with open("config.json", "r") as fl:
        conf = json.loads(fl.read())

        if 'video_source' in conf:
            video_source = conf['video_source']

        if 'ip' in conf:
            ip = conf['ip']

        if 'image_server_ip' in conf:
            image_server_ip = conf['image_server_ip']

        if 'model_path' in conf:
            model_path = conf['model_path']

        if 'zoom' in conf:
            zoom = conf['zoom']

print("Connecting to ip: " + str(ip))

db_settings = DataBaseSettings(ip, db_name, orient_user, orient_pass, db_user, db_pass)


class SessionUser():
    def __init__(self, db, user, emb):
        self.user_id = user['user']
        self.user_name = user['firstName'] + " " + user['lastName']
        self.emb = emb
        self.currentSession = None
        self.db = db
        self.last_update = time.time()

    def getSession(self, image):
        if self.currentSession is None:
            self.currentSession = SessionObject(image, 'webcam')
            self.currentSession.user = self.user_id
        return self.currentSession

    def addData(self, emb, image_id):
        session = self.getSession(image_id)
        new_dist_object = NearDistanceObject(None, None, emb, image_id, session=session)
        session.embeddings.append(new_dist_object)

        self.last_update = time.time()

    def saveSession(self):
        if self.currentSession != None:
            self.currentSession.StoreData(self.db, True)


class MtcnnThread(Thread):
    mtcnn = None

    def __init__(self):
        Thread.__init__(self)

        self.mtcnn = MtcnnBridge()
        self.mtcnn.init()
        print("MtcNN ready")

    def run(self):
        print("Mtcnn thread: run")

        zoom = 4

        while True:
            frame_id, image_tensor = images_queue.get()

            image_tensor = cv2.cvtColor(image_tensor, cv2.COLOR_BGR2RGB)

            last_time = time.time()

            height = np.size(image_tensor, 0)
            width = np.size(image_tensor, 1)

            print(str(height) + ":" + str(width))

            if zoom != 1:
                image_tensor_resized = cv2.resize(image_tensor, dsize=(int(width / zoom), int(height / zoom)), interpolation=cv2.INTER_CUBIC)
            else:
                image_tensor_resized = image_tensor

            result = self.mtcnn.align(image_tensor_resized, 40)
            areas = result[0]
            landmarks = result[1]

            areasCount = len(areas)

            positions = []
            images = []

            for i in range(areasCount):
                pos = areas[i]
                # nose_x = landmarks[2][i]
                # nose_y = landmarks[7][i]

                x = max(int(pos[0] * zoom), 0)
                y = max(int(pos[1] * zoom), 0)
                w = min(int(pos[2] * zoom), width)
                h = min(int(pos[3] * zoom), height)
                tmp = list(map(lambda x: x[i] * zoom, landmarks))
                points = []
                for j in range(int(len(tmp)/2)):
                    points.append({'x':tmp[j], 'y': tmp[j+5]})

                positions.append({
                    'location': {
                        'x': x,
                        'y': y,
                        'w': w - x,
                        'h': h - y,
                        'points': points
                    }
                })

                img = image_tensor[y:h, x:w, 0:3]

                img = cv2.resize(img, dsize=(160, 160), interpolation=cv2.INTER_CUBIC)
                images.append(img)

            print("Mtcnn took %.4f seconds." % (time.time() - last_time))

            if faces_queue.empty():
                faces_queue.put((frame_id, images, positions))


class FacenetThread(Thread):
    facenet = None

    def __init__(self, model_path):
        Thread.__init__(self)

        self.facenet = FacenetBridge(model_path)
        self.facenet.init()
        print("Facenet ready")

    def run(self):
        print("Facenet thread: run")

        while True:
            frame_id, faces, positions = faces_queue.get()

            last_time = time.time()

            embeddings = self.facenet.embeddings(faces)
            for ind, emb in enumerate(embeddings):
                print("ok")
                # self.trackSessions(emb, images[ind], positions[ind])

            print("Facenet took %.4f seconds." % (time.time() - last_time))

            if len(embeddings) > 0:
                embeddings_queue.put((frame_id, embeddings, positions, faces))
                responses_embedding_queue.put((frame_id, embeddings, positions))

class UsersThread(Thread):

    def __init__(self, db_settings: DataBaseSettings):
        Thread.__init__(self)

        self.db = DataBase(db_settings)
        self.users = self.db.getUserMainEmbeddings('average')
        self._embeddingsData, self._tree = self._createKDTree()
        self._treeTimestamp = time.time()
        self.selected_users = []
        self.selectedTimestamp = time.time()
        self.embeddings_counts = {}

        print("Users count: " + str(len(self.users)))

        print("Users ready")

    def _createKDTree(self):
        last_time = time.time()
        embeddingsData = self.db.getAllConfirmedEmbeddings('average')
        embeddings = []
        for item in embeddingsData:
            embeddings.append(list(map(float, item['embedding'])))
        tree = KDTree(embeddings)
        print("createKDTree took %.4f seconds." % (time.time() - last_time))

        return (embeddingsData, tree)

    def getTree(self):
        last_time = time.time()
        if self._treeTimestamp == None or last_time - self._treeTimestamp > recreateKDTreeInterval:
            self._embeddingsData, self._tree = self._createKDTree()
            self._treeTimestamp = last_time

        return self._tree

    def getAllUsers(self):
        all_users = []

        for user in self.users:
            user_name = user['firstName'] + " " + user['lastName']

            emb_object = {
                'user_id': user['user'],
                'user_name': user_name,
                'first_name': user['firstName'],
                'last_name': user['lastName'],
                # 'emb': user['embedding'],
            }
            all_users.append(emb_object)

        if len(all_users) > 0:
            all_users = sorted(all_users, key=lambda x: x['last_name'])

        return all_users

    def getNearDistanceFromUsers(self, emb, position, multy=False):
        kd_tree = self.getTree()
        found_dists, indexes = kd_tree.query(emb, 5, 0, 2, 1.0)
        nearDistances = []

        i = 0
        while i < len(indexes) and indexes[i] < len(self._embeddingsData):
            item = self._embeddingsData[indexes[i]]
            # dist = distance.euclidean(item['embedding'], emb)
            nearDistances.append({
                'user_id': item['userRid'],
                'user_name': item['firstName'] + " " + item['lastName'],
                'emb': item['embedding'],
                'position': position,
                'dist': found_dists[i]
            })
            i += 1

        if len(nearDistances) > 0:
            new_list = sorted(nearDistances, key=lambda x: x['dist'])

            if multy:
                return new_list
            else:
                return new_list[0]
        else:
            return None

    def getNearDistanceFromSelectedUser(self, emb, position):

        for i in range(len(self.selected_users)):
            selected_user = self.selected_users[i]

            dist = distance.euclidean(selected_user.emb, emb)
            if dist < 0.8:
                return {
                    'user_id': selected_user.user_id,
                    'user_name': selected_user.user_name,
                    'emb': selected_user.emb,
                    'position': position,
                    'dist': dist,
                    'learning': True
                }

        return None

    def run(self):
        print("Users thread: run")

        while True:

            if not embeddings_queue.empty():
                frame_id, embs, positions, faces = embeddings_queue.get()

                last_time = time.time()

                users = []
                for i in range(len(embs)):

                    near_user = self.getNearDistanceFromSelectedUser(embs[i], positions[i])
                    if near_user is not None:
                        near_users = [near_user]
                        embedding_count = None if near_user['user_id'] not in self.embeddings_counts else self.embeddings_counts[near_user['user_id']]
                        if embedding_count == None or last_time - embedding_count['timestamp'] > 10.0:
                            embedding_count = self.db.getUserEmbeddingCounts(near_user['user_id'])
                            self.embeddings_counts[near_user['user_id']] = embedding_count
                        user_session = self.getUserSession(near_user['user_id'])
                        if user_session is not None and user_session.currentSession is not None:
                            embedding_count['current'] = len(user_session.currentSession.embeddings)
                        near_user['counts'] = embedding_count

                        accumulation_queue.put(('new', (frame_id, near_user, faces[i])))
                    else:
                        near_users = self.getNearDistanceFromUsers(embs[i], positions[i], True)

                    users.append(near_users)

                print("Users took %.4f seconds." % (time.time() - last_time))

                responses_users_queue.put((frame_id, users))
            if not selected_users_queue.empty():
                action, data = selected_users_queue.get()
                if action == 'stop':
                    user_id = data
                elif action == 'new':
                    user_id, emb = data

                user_session = self.getUserSession(user_id)
                if action == 'new':
                    if user_session is None:
                        selected_user = next((user for user in self.users if user['user'] == user_id), None)

                        if selected_user is not None:
                            session_user = SessionUser(self.db, selected_user, emb)
                            self.selected_users.append(session_user)
                    else:
                        user_session.emb = emb
                elif action == 'stop' and user_session is not None:
                    user_session.saveSession()
                    self.selected_users.remove(user_session)

            if not stored_image_queue.empty():
                frame_id, user, stored_image_id, stored_image_name = stored_image_queue.get()
                user_id = user['user_id']
                emb = user['emb']
                user_session = self.getUserSession(user_id)
                if user_session != None:
                    user_session.addData(emb, stored_image_id)
                else:
                    accumulation_queue.put(('delete', (frame_id, user, stored_image_id, stored_image_name)))

            self.closeOldSelected()

    def getUserSession(self, user_id):
        userSession = next((user for user in self.selected_users if user.user_id == user_id), None)
        return userSession

    def closeOldSelected(self, ttl = 30):
        now = time.time()
        if now - self.selectedTimestamp > ttl/3:
            for user_session in self.selected_users:
                if now - user_session.last_update > ttl:
                    user_session.saveSession()
                    self.selected_users.remove(user_session)
            self.selectedTimestamp = now

    def closeAllSelected(self):
        for user_session in self.selected_users:
            user_session.saveSession()
        self.selected_users = []


class AccumulationThread(Thread):

    def __init__(self, db_settings: DataBaseSettings):
        Thread.__init__(self)
        self.db = DataBase(db_settings)
        print("Accumulation ready")

    def run(self):
        print("Accumulation thread: run")

        while True:
            action, data = accumulation_queue.get()

            if action == 'new':
                frame_id, user, face = data
                last_time = time.time()

                image_db_name = self.storeImage(user, face)
                if image_db_name is not None:
                    image_db_id = self.db.addImage(image_db_name + ".jpg")
                    stored_image_queue.put((frame_id, user, image_db_id, image_db_name))

                print("Accumulation took %.4f seconds." % (time.time() - last_time))

            elif action == 'delete':
                frame_id, user, image_db_id, image_db_name = data
                self.deleteImage(user, image_db_id, image_db_name)

    def array2PIL(self, arr, size):
        mode = 'RGB'
        arr = arr.reshape(arr.shape[0] * arr.shape[1], arr.shape[2])
        # if len(arr[0]) == 3:
        # arr = np.c_[arr, 255*np.ones((len(arr),1), np.uint8)]
        return Image.frombuffer(mode, size, arr.tostring(), 'raw', mode, 0, 1)

    def storeImage(self, user, image):

        image_id = str(time.time())
        user_id = user['user_id']

        url = "http://" + image_server_ip + ":7777/api/" + user_id.replace(":", "-") + "/" + image_id

        print("POST image: " + url)

        im = self.array2PIL(image, (160, 160))
        # im = im.convert("RGB")

        fp = BytesIO()
        format = Image.registered_extensions()['.jpg']
        im.save(fp, format)
        data = fp.getvalue()

        try:
            result = requests.post(url, data=data)
        except ConnectionResetError:
            print("ConnectionResetError")
            return None
        except ConnectionError:
            print("ConnectionError to data server")
            return None
        except:
            print("Uncknown error")
            return None

        return image_id

    def deleteImage(self, user, image_id, image_name):
        user_id = user['user_id']

        url = "http://" + image_server_ip + ":7777/api/delete/" + user_id.replace(":", "-") + "/" + image_name

        print("POST image delete: " + url)

        try:
            result = requests.post(url, {'image_id': image_id})
        except ConnectionResetError:
            print("ConnectionResetError")
            return False
        except ConnectionError:
            print("ConnectionError to data server")
            return False
        except:
            print("Uncknown error")
            return False

        return True


if __name__ == '__main__':
    mtcnn_thread = MtcnnThread()
    facenet_thread = FacenetThread(model_path)
    users_thread = UsersThread(db_settings)
    accumulation_thread = AccumulationThread(db_settings)

    mtcnn_thread.start()
    facenet_thread.start()
    users_thread.start()
    accumulation_thread.start()


class Responser():
    def __init__(self):
        self.responses = {}
        self.last_cleaning = time.time()

    def addUsers(self, frame_id, users):
        if frame_id not in self.responses:
            self.responses[frame_id] = {}
        self.responses[frame_id]['users'] = users
        self.responses[frame_id]['last_update'] = time.time()

    def addEmbeddings(self, frame_id, positions, embeddings):
        if frame_id not in self.responses:
            self.responses[frame_id] = {}
        self.responses[frame_id]['positions'] = positions
        self.responses[frame_id]['embeddings'] = embeddings
        self.responses[frame_id]['last_update'] = time.time()

    def clearOldData(self, ttl=600):
        now = time.time()
        if now - self.last_cleaning > ttl/3:
            for item in self.responses:
                if 'last_update' in self.responses[item]:
                    last_update = self.responses[item]['last_update']
                    if now - last_update > ttl:
                        del self.responses[item]
            self.last_cleaning = now

    def getReadyResponse(self):
        self.clearOldData()
        for item in self.responses:
            if 'users' in self.responses[item] and 'positions' in self.responses[item] and 'embeddings' in self.responses[item]:
                response = self.responses[item]
                del self.responses[item]
                del response['last_update']
                return (item, response)

        return (None, None)

responser = Responser()

class CorevisionHTTPRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        print(self.path)

        if self.path[:4] != '/api':
            self.get_static()
        elif self.path[:15] == '/api/init-users':
            last_time = time.time()
            users_thread.closeAllSelected()
            body = json.dumps({
                'users': users_thread.getAllUsers()
            })

            self.send_response(200)
            self.end_headers()

            response = BytesIO()
            response.write(str.encode(body))
            self.wfile.write(response.getvalue())

            print("GET all users took %.4f seconds." % (time.time() - last_time))
        else:
            self.send_response(200)
            self.end_headers()
            self.wfile.write(b'Hello, world!')

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        body = self.rfile.read(content_length).decode("utf-8")

        if self.path == '/api/select-user':
            try:
                data = json.loads(body)

                user_id = None
                if 'user_id' in data:
                    user_id = data['user_id']

                emb = None
                if 'emb' in data:
                    emb = data['emb']

                selected_users_queue.put(('new', (user_id, emb)))

                self.send_response(200)
                self.end_headers()
                self.wfile.write(b'ok')
            except:
                e = sys.exc_info()
                print(e)
                self.file_not_found()
                return
        elif self.path == '/api/deselect-user':
            try:
                data = json.loads(body)

                user_id = None
                if 'user_id' in data:
                    user_id = data['user_id']

                selected_users_queue.put(('stop', user_id))

                self.send_response(200)
                self.end_headers()
                self.wfile.write(b'ok')
            except:
                e = sys.exc_info()
                print(e)
                self.file_not_found()
                return
        else:
            try:
                last_time = time.time()

                data = json.loads(body)

                if 'command' in data:
                    command = data['command']
                else:
                    self.file_not_found()
                    return

                hash_image = None

                if command == 1:
                    if 'image' in data:
                        image = data['image']
                    else:
                        self.file_not_found()
                        return

                    if image[:22] == 'data:image/jpeg;base64':
                        im = image[23:]
                        image_raw = base64.b64decode(im)
                        if 'frame_id' in data:
                            hash_image = data['frame_id']
                        else:
                            hash_image = hashlib.md5(image_raw).hexdigest()
                        image_tensor = cv2.imdecode(np.fromstring(image_raw, dtype=np.uint8), -1)

                        images_queue.put((hash_image, image_tensor))

                if not responses_users_queue.empty():
                    frame_id, users = responses_users_queue.get()
                    responser.addUsers(frame_id, users)

                if not responses_embedding_queue.empty():
                    frame_id, embeddings, positions = responses_embedding_queue.get()
                    embeddings = embeddings.tolist()
                    responser.addEmbeddings(frame_id, positions, embeddings)

                self.send_response(200)
                self.end_headers()

                frame_id, response = responser.getReadyResponse()
                if response is not None:
                    body = json.dumps({'recived_frame_id': hash_image, 'sent_frame_id': frame_id, 'data': response})
                else:
                    body = '{"recived_frame_id": "%s"}' % hash_image
                bytesIO = BytesIO()
                bytesIO.write(str.encode(body))
                self.wfile.write(bytesIO.getvalue())

                print("POST took %.4f seconds." % (time.time() - last_time))

            except:
                e = sys.exc_info()
                print(e)
                self.file_not_found()
                return

    def get_static(self):
        root = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), ROOT_PATH)

        if self.path == '/':
            filename = root + '/index.html'
        else:
            filename = root + self.path

        print(filename)

        try:
            with open(filename, 'rb') as fh:
                file_data = fh.read()
                # html = bytes(html, 'utf8')

                self.send_response(200)
                if filename[-4:] == '.css':
                    self.send_header('Content-type', 'text/css')
                elif filename[-5:] == '.json':
                    self.send_header('Content-type', 'application/javascript')
                elif filename[-3:] == '.js':
                    self.send_header('Content-type', 'application/javascript')
                elif filename[-4:] == '.ico':
                    self.send_header('Content-type', 'image/x-icon')
                else:
                    self.send_header('Content-type', 'text/html')
                self.end_headers()
                self.wfile.write(file_data)

        except FileNotFoundError:
            self.file_not_found()

    def file_not_found(self):
        self.send_response(404)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

httpd = HTTPServer((HTTP_SERVER_IP, 8000), CorevisionHTTPRequestHandler)

print("Http server listening on port 8000")

httpd.serve_forever()

mtcnn_thread.join()
facenet_thread.join()
users_thread.join()
accumulation_thread.join()
