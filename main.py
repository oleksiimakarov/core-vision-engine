import os
import os.path
import json
from corevision.image_grabber import ImageGrabber
from corevision.facenet_main import FacenetMain
from corevision.mtcnn_main import MtcnnMain
from corevision.data_queue import DataQueue
from corevision.database import DataBaseSettings
from corevision.sound_main import SoundMain
from corevision.web_hooks import WebHooks


# basic config
video_source = 0

ip = os.getenv('COREVISION_SERVER', "localhost")
model_path = os.getenv('FACENET_MODEL', "../model/")
google_credentials = os.getenv("GOOGLE_APPLICATION_CREDENTIALS", "google_credentials.json")
camera_credentials = {"ip":"10.10.6.143", "user":"2way", "password":"1q2w3e4r", "use_sound": False}

db_name = "corevision"

orient_user = "root"
orient_pass = "root"

db_user = "admin"
db_pass = "admin"

images_path = "../images/"
sound_path = "../sound/"

zoom = 4


# read config from json-file
if os.path.exists("config.json"):
    with open("config.json", "r") as fl:
        conf = json.loads(fl.read())

        if 'video_source' in conf:
            video_source = conf['video_source']

        if 'ip' in conf:
            ip = conf['ip']

        if 'model_path' in conf:
            model_path = conf['model_path']

        if 'zoom' in conf:
            zoom = conf['zoom']

        if 'google_credentials' in conf:
            google_credentials = conf['google_credentials']

        if 'ip_cam' in conf:
            camera_credentials = conf['ip_cam']

if video_source == "ip_cam":
    video_source = "rtsp://{user}:{password}@{ip}".format(**camera_credentials)

google_credentials_json_str = None


print("Connecting to ip: " + str(ip))

# open database
db_settings = DataBaseSettings(ip, db_name, orient_user, orient_pass, db_user, db_pass)


# create pipeline and threads
sound_count = 2 if "use_sound" in camera_credentials and camera_credentials['use_sound'] else 0
dataQueue = DataQueue(frames_count=2, faces_count=2, users_count=2, sound_count=sound_count)

# create settings service
grabberInstance = ImageGrabber(video_source, zoom, dataQueue)
mtcnn = MtcnnMain(zoom, dataQueue, db_settings)
fn = FacenetMain(model_path, images_path, dataQueue, db_settings, "ip_cam")
webHookInstance = WebHooks(dataQueue, db_settings)


# start threads
grabberInstance.start()
mtcnn.start()
fn.start()
webHookInstance.start()

if "use_sound" in camera_credentials and camera_credentials['use_sound']:
    sound = SoundMain(sound_path, google_credentials, camera_credentials, dataQueue)
    sound.start()
    sound.join()

# join to main thread
webHookInstance.join()
fn.join()
mtcnn.join()
grabberInstance.join()