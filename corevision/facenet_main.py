import threading
import scipy.misc
from corevision.data_queue import DataQueue
from corevision.nn_bridge import FacenetBridge
from corevision.embedding_object import NearDistanceObject, EmbeddingObject, SessionObject
from corevision.database import DataBase, DataBaseSettings
from corevision.web_hook_user_object import WebHookUserObject
from corevision.utils import Utils
from corevision.settings import Settings

from scipy.spatial import distance

import time
import datetime


class FacenetMain(threading.Thread):
    in_progress = 0

    def __init__(self, model_path, images_path, dataQueue: DataQueue, db_settings: DataBaseSettings, source=None):
        threading.Thread.__init__(self)

        self.db = DataBase(db_settings)
        self.settings = Settings(self.db)
        self.is_data_accumulation_enabled = False
        self.min_image_focus = 30
        self.min_embeddings_count = 2
        self.model_path = model_path
        self.images_path = images_path
        self.frames = dataQueue.frames
        self.faces = dataQueue.faces
        self.queue_users = dataQueue.users
        self.sound = dataQueue.sound if hasattr(dataQueue, 'sound') else False
        self.source = source
        self.users = []
        self.sessions = []
        self.loadUsers()
        self.initNN()

    def updateSettings(self):
        facenet_settings = self.settings.get_facenet_settings()
        self.is_data_accumulation_enabled = facenet_settings['isDataAccumulationEnabled']
        self.min_image_focus = facenet_settings['minImageFocus']
        self.min_embeddings_count = facenet_settings['minEmbeddingsCount']

    def loadUsers(self):
        self.users = self.db.getUserMainEmbeddings('average')
        for user in self.users:
            user['embedding'] = list(map(float, user['embedding']))
        #print(self.users[0])

    def initNN(self):
        self.face = FacenetBridge(self.model_path)
        self.face.init()

    def createNewSession(self):
        print('New session')
        return time.time()

    def closeOldSessions(self):
        for session in self.sessions:
            # if (time.time() - session.session_id) > 3.0 or len(session.embeddings) == 10:
            if (time.time() - session.last_update) > 6.0:
                self.verify_session_embeddings(session)

                if (session.image is not None) and (len(session.embeddings) >= self.min_embeddings_count):
                    session.StoreData(self.db)
                    #TODO else remove stored images

                if hasattr(session, 'user'):
                    user = session.user

                    self.db.addUserHistory(user)

                    if not self.queue_users.full():
                        self.queue_users.put(WebHookUserObject(user, session.embeddings[0].emb.tolist()))

                self.sessions.remove(session)

                print('Close session. Count of opened sessions: ' + str(len(self.sessions)))
                continue

    def trackSessions(self, emb, image_array, position):
        if self.is_data_accumulation_enabled:
            # store image to file system
            image_path = self.saveImageToDisk(image_array)

            # add image path to db
            image = self.db.addImage(image_path)
        else:
            image = None

        # find near embeddings from active sessions
        dist_object = self.getNearDistanceFromActiveSession(emb, position)

        if dist_object is not None:
            # save embedding object to one of active sessions
            dist_object.image = image
            print("Save new embedding to OLD session.")
        else:
            # Save new embedding to new session
            print(self.getName() + " Save new embedding to new session. Add first image")

            session = SessionObject(image, self.source)
            self.sessions.append(session)

            # find embedding in users
            dist_object = self.getNearDistanceFromUsers(emb, position)
            if dist_object is not None:
                # save known face in new session

                dist_object.emb = emb
                dist_object.image = image
                dist_object.setSession(session)
            else:
                # save unknown face in new session
                dist_object = NearDistanceObject(None, None, emb, image, position, 100.0, session)

            if self.sound != False:
                if dist_object.user is not None:
                    self.sound.put({"user": dist_object.user, "message": "Hello. You look like %s. Please confirm that." % dist_object.user_name})
                # else:
                #     self.sound.put({"user": dist_object.user, "message": "I don't know who you are. Please say me your name."})

        EmbeddingObject(dist_object)

    def saveImageToDisk(self, image):
        tm = time.time()
        file_name = str(tm) + '.jpg'
        scipy.misc.imsave(self.images_path + file_name, image)
        return file_name

    def isNearLocation(self, prev_position, new_position):
        _l = prev_position[0]
        _t = prev_position[1]
        _r = prev_position[2]
        _b = prev_position[3]

        _nl = new_position[0]
        _nt = new_position[1]
        _nr = new_position[2]
        _nb = new_position[3]

        __x = _l + ((_r - _l) / 2)
        __y = _t + ((_b - _t) / 2)

        result = (__x > _nl) and (__x < _nr) and (__y > _nt) and (__y < _nb)

        return result


    def getNearDistanceFromActiveSession(self, emb, position):
        nearDistances = []

        for session in self.sessions:
            for user in session.embeddings:
                dist = distance.euclidean(user.emb, emb)
                #if dist < 1.0 and self.isNearLocation(user_position, position):
                if dist < 1.0:
                    emb_object = NearDistanceObject(user.user, user.user_name, emb, None, position, dist, session)
                    nearDistances.append(emb_object)

        if len(nearDistances) > 0:
            new_list = sorted(nearDistances, key=lambda x: x.dist)
            nearest = new_list[0]

            return nearest
        else:
            return None

    def getNearDistanceFromUsers(self, emb, position):
        nearDistances = []

        for user in self.users:
            user_emb = user['embedding']
            user_id = user['user']
            user_name = user['firstName'] + " " + user['lastName']

            dist = distance.euclidean(user_emb, emb)
            if dist < 1.0:
                emb_object = NearDistanceObject(user_id, user_name, emb, None, position, dist)
                nearDistances.append(emb_object)

        if len(nearDistances) > 0:
            new_list = sorted(nearDistances, key=lambda x: x.dist)
            nearest = new_list[0]

            return nearest
        else:
            return None

    def verify_session_embeddings(self, session: SessionObject):
        if len(session.embeddings) > 2:
            verification_list = []
            users_rate = {}
            verification_list.append(session.embeddings[-1])

            def set_user_rate(emb_object: EmbeddingObject):
                if emb_object.user is not None:
                    if emb_object.user in users_rate:
                        users_rate[emb_object.user] += 1
                    else:
                        users_rate[emb_object.user] = 1

            set_user_rate(session.embeddings[0])

            if len(session.embeddings) > 3:
                verification_list.insert(0, Utils.find_middle(session.embeddings))

            for emb_object in verification_list:
                dist_object = self.getNearDistanceFromUsers(emb_object.emb, emb_object.position)
                if dist_object is not None:
                    emb_object.user = dist_object.user
                    emb_object.dist = dist_object.dist
                    set_user_rate(emb_object)
                else:
                    emb_object.user = None

            if len(users_rate) > 0:
                session.user = max(users_rate, key=users_rate.get)  # finding user with highest rate
            else:
                session.user = None

        else:
            session.user = session.embeddings[0].user


    def run(self):
        print("Facenet thread: run")

        while True:

            self.updateSettings()

            # close old sessions
            self.closeOldSessions()

            if not self.faces.empty():
                # if self.faces.full(): # skip owerloaded faces
                    # self.faces.get()

                facesOnScreen = self.faces.get()
                positions = facesOnScreen[0]
                images = facesOnScreen[1]

                #print("detection... {0}".format(str(len(images))))

                embeddings = self.face.embeddings(images)
                for ind, emb in enumerate(embeddings):
                    image_focus_value = Utils.get_image_focus_value(images[ind])
                    if image_focus_value > self.min_image_focus:
                        self.trackSessions(emb, images[ind], positions[ind])
                    else:
                        print("bad image focus value" + str(image_focus_value))

                b = datetime.datetime.now()
                #print("Facenet: " + str(b - a))
            #else:
                #print("Facenet Idle")
            time.sleep(0.01)
