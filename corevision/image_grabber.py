import threading
from corevision.data_queue import DataQueue
import cv2
import numpy as np
import datetime
import time

class ImageGrabber(threading.Thread):
    def __init__(self, ID, zoom, dataQueue: DataQueue):
        threading.Thread.__init__(self)
        self.ID = ID
        self.zoom = zoom
        self.frames = dataQueue.frames
        self.restartGrabber()

    def restartGrabber(self):
        if(hasattr(self, "cam")):
            self.cam.release()
            time.sleep(0.05)
            
        self.cam=cv2.VideoCapture(self.ID)
        time.sleep(0.05)
        
        
    def run(self):
        print("Image grabber thread: run")
       
        
        while True:
            ret, frame=self.cam.read()
            if (ret):
                if(not self.frames.empty()):
                    time.sleep(0.001)
                    continue
                    
                # remove extra frames                
                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                height = np.size(frame, 0)
                width = np.size(frame, 1)
                frame2 = cv2.resize(frame, dsize=(int(width / self.zoom), int(height / self.zoom)), interpolation=cv2.INTER_CUBIC)
                
                if (self.frames.empty()):
                    a = datetime.datetime.now()
                    self.frames.put([frame, frame2])
                    b = datetime.datetime.now()
                    
            else:
                print('Grabber Idle')
                self.restartGrabber()
                
            time.sleep(0.05)

