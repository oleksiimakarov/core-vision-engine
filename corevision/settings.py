from corevision.database import DataBaseSettings, DataBase


class Settings():

    facenet_settings_update_time = 0
    facenet_settings = None
    min_update_time = 60                # seconds

    def __init__(self, db: DataBase):
        self.db = db

    def get_facenet_settings(self):
        if (self.db.timestamp() - self.facenet_settings_update_time) > self.min_update_time:
            self.facenet_settings = self.db.getFacenetSettings()
            self.facenet_settings_update_time = self.db.timestamp()

        return self.facenet_settings
