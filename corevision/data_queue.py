from multiprocessing import Queue

class DataQueue(object):
    def __init__(self, frames_count, faces_count, users_count, sound_count):
        self.frames = Queue(frames_count)
        self.faces = Queue(faces_count)
        self.users = Queue(users_count)
        if sound_count > 0:
            self.sound = Queue(sound_count)
