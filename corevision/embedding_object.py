import time
from corevision.database import DataBase


class NearDistanceObject(object):
    def __init__(self, user: str, user_name, emb, image=None, position=None, dist=None, session=None):
        self.user = user
        self.user_name = user_name
        self.emb = emb
        self.dist = dist
        self.image = image
        self.position = position
        self.setSession(session)

    def setSession(self, session):
        if session is not None:
            self._session = session
            self.session_id = session.session_id
        else:
            self.session_id = 0.0
            self._session = None

    def getSession(self):
        return self._session


class EmbeddingObject(NearDistanceObject):

    def __init__(self, dist: NearDistanceObject) -> object:
        super(EmbeddingObject, self).__init__(dist.user, dist.user_name, dist.emb, dist.image, dist.position, dist.dist, dist.getSession())

    def setSession(self, session):
        super(EmbeddingObject, self).setSession(session)
        if session is not None:
            session.embeddings.append(self)
            session.last_update = time.time()

    def getSession(self):
        return self._session


class SessionObject(object):

    def __init__(self, image=None, source=None):
        print("New session object")
        self.embeddings = []
        self.image = image
        self.session_id = time.time()
        self.last_update = self.session_id
        self.source = source

    def StoreData(self, db: DataBase, isConfirmed=False):

        user = self.user

        print("Store session. ID: {0}. User: {1}  Embedding count: {2}".format(str(self.session_id), str(user), str(len(self.embeddings))))

        session = db.addSession(self.source, user, self.image, None, isConfirmed)

        for emb in self.embeddings:
            emb_data = emb.emb
            if type(emb_data) is not list:
                emb_data = emb_data.tolist()
            db.addEmbedding(emb_data, session, emb.image)
