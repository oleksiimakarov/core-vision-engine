import threading
import requests
import time
import sys
from corevision.data_queue import DataQueue
from corevision.database import DataBase, DataBaseSettings
from corevision.settings import Settings


class WebHooks(threading.Thread):

    def __init__(self, data_queue: DataQueue, db_settings: DataBaseSettings):
        threading.Thread.__init__(self)

        self.db = DataBase(db_settings)
        self.settings = Settings(self.db)
        self.users = data_queue.users
        self.web_hooks_urls = []
        self.update_web_hooks_urls()

    def update_web_hooks_urls(self):
        facenet_settings = self.settings.get_facenet_settings()
        self.web_hooks_urls = facenet_settings['webHooksUrls']

    def run(self):
        print("Web Hooks thread: run")

        while True:

            self.update_web_hooks_urls()

            if not len(self.web_hooks_urls) == 0:
                user_data = self.users.get()

                for url in self.web_hooks_urls:
                    try:
                        requests.post(url, json=user_data.__dict__)
                    except:
                        e = sys.exc_info()[0]
                        print(e)
                        self.web_hooks_urls.remove(url)

            time.sleep(0.001)
