import threading
from corevision.data_queue import DataQueue
from corevision.nn_bridge import MtcnnBridge
from corevision.settings import Settings
import datetime
import time
import numpy as np
import cv2
from corevision.database import DataBase, DataBaseSettings


class MtcnnMain(threading.Thread):
    
    def __init__(self, zoom, dataQueue: DataQueue, db_settings: DataBaseSettings):
        threading.Thread.__init__(self)
        
        self.db = DataBase(db_settings)
        self.settings = Settings(self.db)

        self.zoom = zoom
        self.frames = dataQueue.frames
        self.faces = dataQueue.faces

        self.initNN()

    def initNN(self):
        self.mtcnn = MtcnnBridge()
        self.mtcnn.init()

    def isFaceAligned(self, pos, noise_x, noise_y):

        p_t = 10.0
        p_b = 10.0
        p_l = 8.0
        p_r = 8.0

        x = pos[0]
        y = pos[1]
        x2 = pos[2]
        y2 = pos[3]

        center_x = (x2 - x) / 2.0
        center_y = (y2 - y) / 2.0

        step_x = (x2 - x) / 100.0
        step_y = (y2 - y) / 100.0

        noise_x = noise_x - x
        noise_y = noise_y - y

        left_range = center_x - (step_x * p_l)
        right_range = center_x + (step_x * p_r)

        top_range = center_y - (step_y * p_t)
        bottom_range = center_y + (step_y * p_b)

        result = (left_range < noise_x) and (right_range > noise_x) and (top_range < noise_y) and (bottom_range > noise_y)

        print(result)

        return result

    def run(self):
        print("Mtcnn thread: run")
        
        while True:
            #if(not self.frames.empty()):
                #remove extra faces
                #if (self.faces.full()): #skip oweloaded faces
                    #self.faces.get()
                
                #if self.frames.full(): #skip owerloaded frames
                    #self.frames.get()
                    
                frame = self.frames.get() 
                
                a = datetime.datetime.now()
                facenet_settings = self.settings.get_facenet_settings()
                result = self.mtcnn.align(frame[1], facenet_settings['minImageSize'])
                areas = result[0]
                landmarks = result[1]

                areasCount = len(areas)

                if not areasCount == 0:
                    fullSize = frame[0]
                    height = np.size(fullSize, 0)
                    width = np.size(fullSize, 1)

                    positions = []
                    embeddings = []

                    for i in range(areasCount):
                        pos = areas[i]
                        noise_x = landmarks[2][i]
                        noise_y = landmarks[7][i]

                        #if not self.isFaceAligned(pos, noise_x, noise_y):
                        #    continue

                        x = max(int(pos[0] * self.zoom), 0)
                        y = max(int(pos[1] * self.zoom), 0)
                        w = min(int(pos[2] * self.zoom), width)
                        h = min(int(pos[3] * self.zoom), height)
                        
                        positions.append([x,y,w,h])
                        
                        img = fullSize[y:h, x:w, 0:3]
                        
                        img = cv2.resize(img, dsize=(160, 160), interpolation=cv2.INTER_CUBIC)  
                        embeddings.append(img)

                    if (not self.faces.full()):
                        current_time = round(time.time(), 2)
                        # cv2.imwrite("../_tmp/frame%f.jpg" % current_time, frame[1])
                        # cv2.imwrite("../_tmp/face%f.jpg" % current_time, embeddings[0])
                        self.faces.put([positions, embeddings])
                        

            #else:
                #print("Mtcnn Idle")
                time.sleep(0.001)
