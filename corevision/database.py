import sys
import pyorient
import time

class DataBaseSettings():
    def __init__(self, ip: str, db_name: str, orient_user: str, orient_pass: str, db_user: str, db_pass: str):
        self.ip = ip
        self.db_name = db_name
        self.orient_user = orient_user
        self.orient_pass = orient_pass
        self.db_user = db_user
        self.db_pass = db_pass


class DataBase():
    user_cluster_id = None  # Users
    session_cluster_id = None  # Sessions
    embedding_cluster_id = None  # Embeddings
    image_cluster_id = None  # Images
    branch_cluster_id = None  # Branch
    user_history_cluster_id = None  # UserHistory
    facenet_settings_cluster_id = None  # UserHistory
    model_cluster_id = None  # Model

    def __init__(self, db_settings: DataBaseSettings):

        # ip, db_name, orientdb_user, orientdb_pass, db_user, db_pass
        self.db_name = db_settings.db_name
        self.client = pyorient.OrientDB(db_settings.ip, 2424)
        while True:
            try:
                self.client.connect(db_settings.orient_user, db_settings.orient_pass)
                self.clusters = self.client.db_open(self.db_name, db_settings.db_user, db_settings.db_pass, pyorient.DB_TYPE_GRAPH, "")

                self.user_cluster_id = self.client.get_class_position(b'user')
                self.session_cluster_id = self.client.get_class_position(b'session')
                self.embedding_cluster_id = self.client.get_class_position(b'embedding')
                self.image_cluster_id = self.client.get_class_position(b'image')
                self.branch_cluster_id = self.client.get_class_position(b'branch')
                self.user_history_cluster_id = self.client.get_class_position(b'userhistory')
                self.facenet_settings_cluster_id = self.client.get_class_position(b'facenetsettings')
                self.model_cluster_id = self.client.get_class_position(b'model')

                print("Database: Connected")
                break
            except pyorient.exceptions.PyOrientConnectionException:
                print("Database: Try to connect...")
                time.sleep(3)
                break
            except:
                e = sys.exc_info()
                print(e[0])
                time.sleep(3)

    def _escape(self, rec_id):
        assert rec_id is not None
        assert rec_id != 0
        assert rec_id != -1
        return rec_id

    def timestamp(self):
        return int(time.mktime(time.gmtime()))

    def recordLink(self, link):
        if link is None:
            return None

        if not (type(link) == 'pyorient.otypes.OrientRecordLink'):
            return pyorient.otypes.OrientRecordLink(str(link))
        else:
            return link

    """
    def prepareRecordWithChildren(self, item):
        data = item.oRecordData
        for i in data:
            field = data[i]

            if type(field) == pyorient.otypes.OrientRecordLink:
                recId = field.clusterID + ":" + field.recordPosition
                try:
                    result = self.client.query("select * from {0} where @rid = '{1}'".format(i, recId))
                    data[i] = result[0].oRecordData
                except Exception:
                    print("Err")
                #print(recId)

        #print(data)
        return data
    """

    def prepareRecord(self, item):
        data = item.oRecordData
        for i in data:
            field = data[i]

            if type(field) == pyorient.otypes.OrientRecordLink:
                rec_id = field.clusterID + ":" + field.recordPosition
                data[i] = rec_id
        return data

    def query(self, query, count=10000):
        return self.client.query(query, count)

    """
    def getUsers(self):
        users = self.client.query("SELECT FROM User",  1000)
        return list(map(self.prepareRecordWithChildren, users))
    """

    def getUserMainEmbeddings(self, source=None):
        sourceCondition = '' if source is None else " and session.source = '%s'" % source
        embs = self.client.query("""SELECT session.user as user, session.user.firstName as firstName, session.user.lastName as lastName, image.path as path, embedding
                       FROM Embedding
                       WHERE session.user IS NOT NULL AND session.isConfirmed = TRUE %s
                       GROUP BY session.user
                       ORDER BY @rid DESC""" % sourceCondition, 10000)
        return list(map(self.prepareRecord, embs))

    def getAllConfirmedEmbeddings(self, source=None):
        sourceCondition = '' if source is None else " and session.source = '%s'" % source
        embeddings = self.client.query("""
            SELECT @rid, session.user AS userRid, session.user.firstName AS firstName, session.user.lastName AS lastName, embedding
            FROM Embedding
            WHERE session.user IS NOT NULL AND session.isConfirmed = TRUE %s""" % sourceCondition, 100000)
        return list(map(self.prepareRecord, embeddings))

    def getConfirmedEmbeddingsCount(self, session_count):
        records = self.client.query("select count(*) from ( SELECT user.@rid AS us, count(isConfirmed) AS conf \
                        FROM session \
                        WHERE user IS NOT NULL AND isConfirmed = true \
                        GROUP BY user.@rid) where conf > " + str(session_count), 1)

        count = records[0].oRecordData['count']
        return count

    def getUserEmbeddingCounts(self, user_rid):
        records = self.client.query("""SELECT COUNT(*) AS conf 
                        FROM embedding 
                        WHERE session.user = '{rid}' AND session.isConfirmed = true""".format(rid=user_rid), 1)
        confirmed_count = records[0].oRecordData['conf']

        records = self.client.query("""SELECT COUNT(*) AS conf 
                        FROM embedding 
                        WHERE session.user = '{rid}' AND session.isConfirmed = false""".format(rid=user_rid), 1)
        unconfirmed_count = records[0].oRecordData['conf']

        return {'confirmed': confirmed_count, 'unconfirmed': unconfirmed_count, 'timestamp':time.time()}

    def getConfirmedEmbeddings2(self, session_count):
        embs = self.client.query("SELECT FROM ( \
                    SELECT EXPAND( $c ) \
                    LET $a = ( \
                        select us, embedding from ( SELECT user.@rid AS us, count(isConfirmed) AS conf \
                        FROM session \
                        WHERE user IS NOT NULL AND isConfirmed = true \
                        GROUP BY user.@rid) where conf > " + str(session_count) + " ),\
                    $b = ( \
                        select session.user as us, embedding from Embedding where session.isConfirmed == true and \
                        session.user.@rid IN ($a) \
                        ), \
                    $c = unionall( $a, $b ) \
                    ) where embedding is not null", 1000000)
        return list(map(self.prepareRecord, embs))

    def getConfirmedEmbeddingsOld(self, session_count: int):
        users = self.client.query("""
            SELECT userRid
            FROM (
                SELECT user AS userRid, count(isConfirmed) AS confirmedCount
                FROM Session
                WHERE user IS NOT NULL AND isConfirmed = TRUE
                GROUP BY user
            )
            WHERE confirmedCount >= {count}
        """.format(count=str(session_count)))

        users = list(map(self.prepareRecord, users))
        embeddings = []

        for user in users:
            user_embeddings = self.client.query("""
                SELECT session.user as us, dateTime, embedding
                FROM Embedding
                WHERE session.isConfirmed = true AND session.user = {user_rid}
                ORDER BY dateTime DESC
                LIMIT {count}
            """.format(count=str(session_count), user_rid=user["userRid"]))
            embeddings = embeddings + user_embeddings

        return list(map(self.prepareRecord, embeddings))

    def getConfirmedEmbeddings(self, session_count: int):
        users = self.client.query("""
            SELECT userRid
            FROM (
                SELECT session.user AS userRid, count(*) AS confirmedCount
                FROM Embedding
                WHERE session.user IS NOT NULL AND session.isConfirmed = TRUE
                GROUP BY session.user
            )
            WHERE confirmedCount >= {count}
        """.format(count=str(session_count)))

        users = list(map(self.prepareRecord, users))
        embeddings = []

        for user in users:
            user_embeddings = self.client.query("""
                SELECT session.user as us, dateTime, embedding
                FROM Embedding
                WHERE session.isConfirmed = true AND session.user = {user_rid}
                ORDER BY dateTime DESC
                LIMIT {count}
            """.format(count=str(session_count), user_rid=user["userRid"]))
            embeddings = embeddings + user_embeddings

        return list(map(self.prepareRecord, embeddings))

    def addSession(self, source, user_id=None, image_id=None, data_time=None, is_confirmed=False):
        if data_time is None:
            data_time = self.timestamp()

        new_session = {
            '@SessionEvent': {
                "dateTime": data_time,
                "source": source,
                "user": self.recordLink(user_id),
                "image": self.recordLink(image_id),
                "isConfirmed": is_confirmed
            }
        }

        rec_position = self.client.record_create(self.session_cluster_id, new_session)
        return self._escape(rec_position._rid)

    def addEmbedding(self, embedding, session_id, image_id=None, data_time=None):

        if data_time is None:
            data_time = self.timestamp()

        new_embedding = {
            '@EmbeddingEvent': {
                "dateTime": data_time,
                "image": self.recordLink(image_id),
                "session": self.recordLink(session_id),
                "embedding": embedding
            }
        }

        rec_position = self.client.record_create(self.embedding_cluster_id, new_embedding)
        return self._escape(rec_position._rid)

    def addUser(self, first_name, last_name, branch_id, image_id=None, level=0):
        new_user = {
            '@UserEvent': {
                "firstName": first_name,
                "lastName": last_name,
                "image": self.recordLink(image_id),
                "branch": self.recordLink(branch_id),
                "recognitionLevel": level
            }
        }

        rec_position = self.client.record_create(self.user_cluster_id, new_user)
        return self._escape(rec_position._rid)

    def addImage(self, path):
        p = path.split('.')
        new_image = {
            '@ImageEvent': {
                "name": p[0] + "." + p[1],
                "path": path
            }
        }

        rec_position = self.client.record_create(self.image_cluster_id, new_image)
        return self._escape(rec_position._rid)

    def addBranch(self, name):
        new_branch = {
            '@BranchEvent': {
                "name": name,
            }
        }

        rec_position = self.client.record_create(self.branch_cluster_id, new_branch)
        return self._escape(rec_position._rid)

    def addUserHistory(self, user_id, data_time=None):
        if data_time is None:
            data_time = self.timestamp()

        new_user_history = {
            '@UserHistoryEvent': {
                "user": self.recordLink(user_id),
                "dateTime": data_time
            }
        }

        rec_position = self.client.record_create(self.user_history_cluster_id, new_user_history)
        return self._escape(rec_position._rid)

    def getFacenetSettings(self):
        settings = self.client.query("SELECT isDataAccumulationEnabled, isClassificatorEnabled, minSessionsCount, webHooksUrls, minImageFocus, minEmbeddingsCount, minImageSize FROM FacenetSettings", 1)
        if len(settings) == 0:
            return {
                "isDataAccumulationEnabled": False,
                "isClassificatorEnabled": False,
                "minSessionsCount": 30,
                "webHooksUrls": [],
                "minImageFocus": 30,
                "minEmbeddingsCount": 2,
                "minImageSize": 80
            }
        else:
            return settings[0].oRecordData

    def addModel(self, id, classes, accuracy):
        new_model = {
            '@ModelEvent': {
                "id": id,
                "classes": classes,
                "accuracy": accuracy
            }
        }

        rec_position = self.client.record_create(self.model_cluster_id, new_model)
        return self._escape(rec_position._rid)

    def getModels(self):
        models = self.client.query('select * from Model order by @rid desc', 10000)
        return list(map(self.prepareRecord, models))

    def getLastModel(self):
        models = self.client.query('select * from Model order by @rid desc', 1)
        if len(models) == 0:
            return None
        else:
            return self.prepareRecord(models[0])
