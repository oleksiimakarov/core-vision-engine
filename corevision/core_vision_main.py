import threading
import time

from corevision.database import DataBase
from corevision.data_queue import DataQueue
from corevision.core_vision_classifier import CoreVision


class CoreVisionMain(threading.Thread):
    def __init__(self, data_queue: DataQueue, db: DataBase, new_model=False):
        threading.Thread.__init__(self)
        self.min_items_count_per_class = 100
        self.last_count_of_classes = 0

        self.db = db
        self.data_queue = data_queue

        self.models_loaded = False
        self.new_model_training = False

        self.loaded_model = None

        self.new_model = new_model

    def load_last_model(self):
        print("Load model: ")
        model_record = self.db.getLastModel()
        if model_record is not None:
            id = model_record["id"]
            classes = model_record["classes"]
            self.loaded_model = CoreVision(self.db)
            self.loaded_model.load_model(id, classes)
            self.loaded_model.build_model()
            self.loaded_model.restore_model()

    def run(self):
        print("CoreVision thread: run")

        last_time = time.time()

        while True:
            print("run")

            if time.time() > last_time + 60 and self.new_model_training is False:
                # check if new users appear for creating new classifier model
                max_groups = self.db.getConfirmedEmbeddingsCount(self.min_items_count_per_class)
                if max_groups != self.last_count_of_classes:
                    # new users appear
                    self.rebuild_classificator()

            time.sleep(60)

    def rebuild_model(self):
        self.new_model_training = True

        # create new classifier
        new_cv = CoreVision(self.db)

        new_cv.load_embeddings(100)
        new_cv.prepare_data_for_training()
        new_cv.build_model()
        new_cv.train_model()

        self.new_model_training = False
