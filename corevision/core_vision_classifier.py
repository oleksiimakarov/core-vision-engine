import time
import sklearn.model_selection as sk
import pandas as pd
import tensorflow as tf
import numpy as np
from corevision.database import DataBase


MAX_EDGES = 30
BATCH_SIZE = 4

NETWORK_WIDTH = 600


class CoreVision():
    def __init__(self, db: DataBase):
        self.db = db
        self.batch_size = BATCH_SIZE

        self.classes = []
        self.classes_count = 0
        self.model_id = str(time.time())
        self.model_name = '../models/' + self.model_id + '/model'

    def load_model(self, model_id=None, classes=None):
        self.model_id = model_id
        self.classes = classes

        self.classes_count = len(self.classes)
        self.model_name = '../models/' + self.model_id + '/model'


    def load_embeddings(self, count=100):
        self.data = self.db.getConfirmedEmbeddings(count)

    def train_input_fn(self, features, labels, batch_size):
        """An input function for training"""
        # Convert the inputs to a Dataset.
        dataset = tf.data.Dataset.from_tensor_slices((features, labels))

        # Shuffle, repeat, and batch the examples.
        dataset = dataset.shuffle(10000).repeat().batch(batch_size)

        # Build the Iterator, and return the read end of the pipeline.
        return dataset.make_one_shot_iterator()

    
    def prepare_labels(self, labels, classes):
        labels_count = len(labels)
        group_count = len(classes)

        new_labels = np.zeros((labels_count, group_count))

        for pos in range(labels.shape[0]):
            val = labels[pos]
            index = np.where(classes == val)
            new_labels[pos, index] = 1.

        return new_labels

    def prepare_data_for_training(self):
        dataframe = pd.DataFrame(self.data, columns=['us', 'embedding'])
        self.classes = dataframe['us'].unique()
        self.classes_count = len(self.classes)

        labels = dataframe.iloc[:, 0:1].values
        lists = dataframe.iloc[:, 1:2].values

        all_records_count = np.size(lists, 0)

        labels = np.reshape(labels, all_records_count)
        lists = np.reshape(lists, all_records_count)
        features=np.array([xj for xj in lists], ndmin=2)

        labels = self.prepare_labels(labels, self.classes)

        X_train, X_test, y_train, y_test = sk.train_test_split(features, labels, test_size=0.30, random_state = 42)

        self.train_records_count = np.size(y_train, 0)
        self.train = self.train_input_fn(X_train, y_train, self.batch_size)

        test_records_count = np.size(y_test, 0)
        self.test = self.train_input_fn(X_test, y_test, test_records_count)

        print('group count %g' % self.classes_count)
        print('train record count %g' % self.train_records_count)
        print('test record count %g' % test_records_count)

    
    def weight_variable(self, shape):
        initial = tf.truncated_normal(shape, stddev=0.01)
        return tf.Variable(initial)


    def bias_variable(self, shape):
        initial = tf.constant(0.0, shape=shape)
        return tf.Variable(initial)
    
    
    def build_model(self, allow_dropouts=False, normalize_data=True):
        # correct labels
        self.y_ = tf.placeholder(tf.float32, [None, self.classes_count])

        # input data
        _x = tf.placeholder(tf.float32, [None, 128])

        # dropouts
        self.keep_prob_input = tf.placeholder(tf.float32)
        self.keep_prob = tf.placeholder(tf.float32)

        if normalize_data:
            self.x = (_x + 1.0) / 2.0
        else:
            self.x = _x

        # build the network
        if allow_dropouts:
            x_drop = tf.nn.dropout(self.x, keep_prob=self.keep_prob_input)
        else:
            x_drop = self.x

        W_fc1 = self.weight_variable([128, NETWORK_WIDTH])
        b_fc1 = self.bias_variable([NETWORK_WIDTH])
        h_fc1 = tf.nn.relu(tf.matmul(x_drop, W_fc1) + b_fc1)


        if allow_dropouts:
            h_fc1_drop = tf.nn.dropout(h_fc1, self.keep_prob)
        else:
            h_fc1_drop = h_fc1

        W_fc2 = self.weight_variable([NETWORK_WIDTH, NETWORK_WIDTH])
        b_fc2 = self.bias_variable([NETWORK_WIDTH])
        h_fc2 = tf.nn.relu(tf.matmul(h_fc1_drop, W_fc2) + b_fc2)

        if allow_dropouts:
            h_fc2_drop = tf.nn.dropout(h_fc2, self.keep_prob)
        else:
            h_fc2_drop = h_fc2

        W_fc3 = self.weight_variable([NETWORK_WIDTH, NETWORK_WIDTH])
        b_fc3 = self.bias_variable([NETWORK_WIDTH])
        h_fc3 = tf.nn.relu(tf.matmul(h_fc2_drop, W_fc3) + b_fc3)

        if allow_dropouts:
            h_fc3_drop = tf.nn.dropout(h_fc3, self.keep_prob)
        else:
            h_fc3_drop = h_fc3

        W_fc4 = self.weight_variable([NETWORK_WIDTH, self.classes_count])
        b_fc4 = self.bias_variable([self.classes_count])
        y = tf.nn.softmax(tf.matmul(h_fc3_drop, W_fc4) + b_fc4)

        # define the loss function
        cross_entropy = tf.reduce_mean(-tf.reduce_sum(self.y_ * tf.log(y), reduction_indices=[1]))

        # define training step and accuracy
        self.train_step = tf.train.MomentumOptimizer(learning_rate=0.01, momentum=0.9).minimize(cross_entropy)
        self.correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(self.y_, 1))

        self.result_predition = tf.argmax(y, 1)

        # probability
        sh_size = tf.shape(y)[0]
        self.pr = tf.reshape(y, [self.classes_count])
        self.probability = tf.slice(self.pr, [tf.argmax(self.pr)], [1])[0]

        # accuracy
        self.accuracy = tf.reduce_mean(tf.cast(self.correct_prediction, tf.float32))

        # create a saver
        self.saver = tf.train.Saver()

        # initialize the graph
        init = tf.global_variables_initializer()

        self.sess = tf.Session()
        self.sess.run(init)


    def save_model(self):
        self.saver.save(self.sess, self.model_name)

    def restore_model(self):
        self.saver.restore(self.sess, self.model_name)

    def train_model(self):
        print("Starting the training...")

        # train
        batch_size = BATCH_SIZE

        self.save_model()

        self.train_images, self.train_predictions = self.train.get_next()

        test_images, test_labels = self.test.get_next()
        self.images, self.labels = self.sess.run((test_images, test_labels))


        start_time = time.time()
        best_accuracy = 0.0
        for i in range(int(MAX_EDGES * self.train_records_count / self.batch_size)):

            input_images, correct_predictions = self.sess.run((self.train_images, self.train_predictions))

            if i % int(self.train_records_count / self.batch_size) == 0:
                train_accuracy = self.sess.run(self.accuracy, feed_dict={
                    self.x: input_images,
                    self.y_: correct_predictions,
                    self.keep_prob_input: 1.0,
                    self.keep_prob: 1.0
                })
                print("step %d, training accuracy %g" % (i, train_accuracy))

                # validate
                test_accuracy = self.sess.run(self.accuracy, feed_dict={
                    self.x: self.images,
                    self.y_: self.labels,
                    self.keep_prob_input: 1.0,
                    self.keep_prob: 1.0
                })
                if test_accuracy >= best_accuracy:
                    self.save_model()
                    best_accuracy = test_accuracy

                    print("Validation accuracy improved: %g. Saving the network." % test_accuracy)
                else:
                    # self.restore_model()

                    print("Validation accuracy was: %g. It was better before: %g. " % (test_accuracy, best_accuracy) +
                          "Using the old params for further optimizations.")

                for j in range(int(1)):
                    test_probability = self.sess.run(self.probability, feed_dict={
                        self.x: self.images[:][j:j+1],
                        self.y_: self.labels[:][j:j+1],
                        self.keep_prob_input: 1.0,
                        self.keep_prob: 1.0})
                    print("Validation probability: %g." % test_probability)

            self.sess.run(self.train_step, feed_dict={
                self.x: input_images,
                self.y_: correct_predictions,
                self.keep_prob_input: 0.8,
                self.keep_prob: 0.5
            })

        print("The training took %.4f seconds." % (time.time() - start_time))

        # validate
        print("Best test accuracy: %g" % best_accuracy)

        self.db.addModel(self.model_id, self.classes.tolist(), float(best_accuracy))
        print("Model saved")

    def prediction(self, input_image):

        result, probability, pr = self.sess.run([self.result_predition, self.probability, self.pr], feed_dict={
            self.x: input_image,
            # self.y_: self.classes,
            self.keep_prob_input: 1.0,
            self.keep_prob: 1.0
        })

        return self.classes[result[0]], probability, pr
