import tensorflow as tf
import numpy as np
from typing import (
    Any,
    List,
    Tuple,
)

from corevision.data_queue import DataQueue

import align.detect_face
import facenet
import os



class MtcnnBridge():
    """
    MTCNN Face Alignment
    """
    def __init__(self) -> None:
        self.graph = self.session = None            # type: Any
        self.pnet = self.rnet = self.onet = None    # type: Any

        self.threshold = [0.6, 0.7, 0.7]    # three steps's threshold
        self.factor = 0.709                 # scale factor

    def init(self) -> None:
        """ doc """

        self.graph = tf.Graph()
        self.session = tf.Session(graph=self.graph)

        # pylint: disable=not-context-manager
        with self.graph.as_default():
            with self.session.as_default():
                self.pnet, self.rnet, self.onet = \
                    align.detect_face.create_mtcnn(self.session, None)

    def align(
            self,
            image: np.ndarray,
            min_size_of_face = 80
    ) -> Tuple[List[Any], List[Any]]:
        """ doc """

        #print(image.shape)
        
        bounding_boxes, landmarks = align.detect_face.detect_face(
            image,   # get rid of alpha channel(if any)
            min_size_of_face,
            self.pnet,
            self.rnet,
            self.onet,
            self.threshold,
            self.factor,
        )

        return bounding_boxes.tolist(), landmarks.tolist()

class FacenetBridge(object):
    """
    Bridge of Facenet
    """
    FACENET_MODEL = None   # type: str

    def __init__(self, model_path) -> None:
        self.FACENET_MODEL = model_path

        self.graph = self.session = None        # type: Any

        self.placeholder_input = None           # type: Any
        self.placeholder_phase_train = None     # type: Any
        self.placeholder_embeddings = None      # type: Any

    def init(self) -> None:
        """ doc """
        self.graph = tf.Graph()
        self.session = tf.Session(graph=self.graph)

        # pylint: disable=not-context-manager
        with self.graph.as_default():
            with self.session.as_default():
                model_dir = os.path.expanduser(self.FACENET_MODEL)
                meta_file, ckpt_file = facenet.get_model_filenames(model_dir)
                saver = tf.train.import_meta_graph(
                    os.path.join(model_dir, meta_file),
                )
                saver.restore(
                    tf.get_default_session(),
                    os.path.join(model_dir, ckpt_file),
                )
                # facenet.load_model(self.FACENET_MODEL)

        self.placeholder_input = self.graph.get_tensor_by_name('input:0')
        self.placeholder_phase_train = \
            self.graph.get_tensor_by_name('phase_train:0')
        self.placeholder_embeddings = \
            self.graph.get_tensor_by_name('embeddings:0')

    def embeddings(
            self,
            images: np.ndarray
    ) -> np.ndarray:
        """
        Get embeddings
        """

        slices = np.empty((len(images), 160, 160, 3))
        for i in range(len(images)):
            slices[i,:,:,:] = facenet.prewhiten(images[i])
            
        feed_dict = {
            self.placeholder_input:         slices,
            self.placeholder_phase_train:   False,
        }
        # Use the facenet model to calcualte embeddings
        embeddings = self.session.run(
            self.placeholder_embeddings,
            feed_dict=feed_dict,
        )
        
        # Return the only row
        return embeddings
