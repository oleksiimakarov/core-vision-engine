from speech_recognition import AudioSource


class RawAudioStream(AudioSource):
    def __init__(self, raw_stream, sample_rate, sample_width=2, chunk_size=1024, channels=1):
        assert sample_rate is None or (
                isinstance(sample_rate, int) and sample_rate > 0), "Sample rate must be None or a positive integer"
        assert isinstance(chunk_size, int) and chunk_size > 0, "Chunk size must be a positive integer"

        self.SAMPLE_WIDTH = sample_width
        self.SAMPLE_RATE = sample_rate  # sampling rate in Hertz
        self.CHUNK = chunk_size  # number of frames stored in each buffer

        self.data_stream = raw_stream
        self.stream = None

    def __enter__(self):
        assert self.stream is None, "This audio source is already inside a context manager"
        self.stream = RawAudioStream.RawStream(self.data_stream)
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        try:
            self.stream.close()
        finally:
            self.stream = None

    class RawStream(object):
        def __init__(self, stream):
            self.pipe_stream = stream

        def read(self, size):
            return self.pipe_stream.read(size)

        def close(self):
            self.pipe_stream.close()


class RawAudioData(AudioSource):
    def __init__(self, raw_data, sample_rate, sample_width=2, chunk_size=1024, channels=1):
        assert sample_rate is None or (
                isinstance(sample_rate, int) and sample_rate > 0), "Sample rate must be None or a positive integer"
        assert isinstance(chunk_size, int) and chunk_size > 0, "Chunk size must be a positive integer"

        self.SAMPLE_WIDTH = sample_width
        self.SAMPLE_RATE = sample_rate  # sampling rate in Hertz
        self.CHUNK = chunk_size  # number of frames stored in each buffer

        self.data = raw_data

    def __enter__(self):
        assert self.stream is None, "This audio source is already inside a context manager"
        self.stream = RawAudioData.RawData(self.data_data)
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        pass

    class RawData(object):
        def __init__(self, data):
            self.data = data
            self.pos = 0

        def read(self, size):
            if self.data == None or self.pos == len(self.data):
                return None

            if self.pos + size >= len(self.data):
                size = len(self.data) - self.pos
            result = self.data[self.pos:size]
            self.pos += size
            return result

        def close(self):
            self.data = None
