import threading
import os
import sys
import hashlib
import subprocess
from http.client import HTTPConnection, NotConnected
import collections
from base64 import b64encode
import time
import ffmpeg
from google.cloud import texttospeech
import speech_recognition

from corevision.RawAudioSource import RawAudioStream
from corevision.data_queue import DataQueue


class HTTPConnectionPlus(HTTPConnection):
    def send(self, data, blocksize=512, send_rate=8000):
        """Send `data' to the server with current bitrate (byte/s).
        ``data`` can be a string object, a bytes object, an array object, a
        file-like object that supports a .read() method, or an iterable object.
        """

        if self.sock is None:
            if self.auto_open:
                self.connect()
            else:
                raise NotConnected()

        if self.debuglevel > 0:
            print("send:", repr(data))

        if hasattr(data, "read"):
            if self.debuglevel > 0:
                print("sendIng a read()able")
            encode = False
            try:
                mode = data.mode
            except AttributeError:
                # io.BytesIO and other file-like objects don't have a `mode`
                # attribute.
                pass
            else:
                if "b" not in mode:
                    encode = True
                    if self.debuglevel > 0:
                        print("encoding file using iso-8859-1")

            last_time = time.time()
            interval = blocksize / send_rate
            while 1:
                datablock = data.read(blocksize)
                if not datablock:
                    break
                if encode:
                    datablock = datablock.encode("iso-8859-1")
                self.sock.sendall(datablock)
                past_time = time.time() - last_time
                if interval > past_time:
                    time.sleep(interval - past_time)

            return
        try:
            interval = blocksize / send_rate
            size = len(data)
            pos = 0
            while size > 0:
                last_time = time.time()
                self.sock.sendall(data[pos:pos + blocksize])
                pos += blocksize
                size -= blocksize
                past_time = time.time() - last_time
                if interval > past_time:
                    time.sleep(interval - past_time)
        except TypeError:
            if isinstance(data, collections.Iterable):
                for d in data:
                    self.sock.sendall(d)
            else:
                raise TypeError("data should be a bytes-like object or an iterable, got %r" % type(data))


class SoundMain(threading.Thread):
    def __init__(self, sound_path, google_credentials, camera_credentials, dataQueue: DataQueue = None):
        threading.Thread.__init__(self)
        self.sound_path = sound_path
        self.soundQueue = dataQueue.sound if dataQueue is not None else None
        self.camera_credentials = camera_credentials

        self.cache = {}
        if not google_credentials.startswith("/"):
            google_credentials = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), google_credentials)
        os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = google_credentials

        if os.path.exists(google_credentials):
            with open(google_credentials, "r") as fl:
                self.google_credentials_json = fl.read()

        self.client = texttospeech.TextToSpeechClient()
        self.voice = texttospeech.types.VoiceSelectionParams(
            language_code='en-US',
            ssml_gender=texttospeech.enums.SsmlVoiceGender.NEUTRAL)
        self.audio_config = texttospeech.types.AudioConfig(audio_encoding=texttospeech.enums.AudioEncoding.MP3)

        self.recognizer = speech_recognition.Recognizer()
        self.stop_listening = None

        self.state = {
            "user": None,
            "message": None,
            "is": "free",
            "timestamp": time.time()
        }

    def sendSoundMessage(self, message):
        print('Send message: ' + message)
        file_path = self._synthesizeSound(message)
        file = open(file_path, "rb")
        soundData = file.read()
        file.close()
        self.sendSound(soundData)

    def _synthesizeSound(self, message):
        if message not in self.cache:
            hash_object = hashlib.md5(bytes(message, 'utf-8'))
            tmp_file_name = hash_object.hexdigest() + ".mulaw"
            if os.path.exists(self.sound_path + tmp_file_name):
                self.cache[message] = self.sound_path + tmp_file_name
            else:
                synthesis_input = texttospeech.types.SynthesisInput(text=message)

                response = self.client.synthesize_speech(synthesis_input, self.voice, self.audio_config)
                if not os.path.exists(self.sound_path):
                    os.makedirs(self.sound_path)

                with open(self.sound_path + tmp_file_name, 'wb') as out:
                    command = (
                        ffmpeg
                            .input('-', format='mp3')
                            .output('-', format='mulaw', acodec='pcm_mulaw', ar=8000, ac=1)
                            .compile()
                    )
                    command.insert(1, "-loglevel")
                    command.insert(2, "quiet")
                    pipe = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=out)
                    # pipe.stdin.write(response.audio_content)
                    # pipe.stdin.close()
                    # pipe.terminate()
                    pipe.communicate(response.audio_content, 2)
                    self.cache[message] = self.sound_path + tmp_file_name

        return self.cache.get(message)

    def sendSound(self, soundData):

        print("Start sending sound...")
        userAndPass = "%s:%s" % (self.camera_credentials['user'], self.camera_credentials['password'])
        userAndPass = b64encode(userAndPass.encode('utf-8')).decode("ascii")
        headers = {'Authorization': 'Basic %s' % userAndPass, "Connection": "Keep-Alive"}

        conn = HTTPConnectionPlus(self.camera_credentials['ip'])
        conn.request("PUT", "/ISAPI/System/TwoWayAudio/channels/1/open", None, headers)
        r1 = conn.getresponse()
        # data1 = r1.read()

        headers["Content-Type"] = "application/octet-stream"
        headers["Connection"] = "close"
        conn.request("PUT", "/ISAPI/System/TwoWayAudio/channels/1/audioData", soundData, headers)
        r1 = conn.getresponse()
        # data1 = r1.read()
        conn.close()
        print("Sending was ended")

    def saveSoundToDisk(self, audioData, prefix=''):
        wavData = audioData.get_wav_data()
        if not os.path.exists(self.sound_path + "records/user" + self.state['user']):
            os.makedirs(self.sound_path + "records/user" + self.state['user'])
        file = open("{path}records/user{id}/{prefix}{time}.wav".format(path=self.sound_path, id=self.state['user'], prefix=prefix, time=round(time.time(), 3)), "wb")
        file.write(wavData)
        file.close()

    def openStream(self):
        command = (
            ffmpeg
                .input('rtsp://2way:1q2w3e4r@10.10.6.143/')
                .output('-', format='s16le', acodec='pcm_s16le', ac=1, ar='8000')
                .compile()
        )
        self.pipe = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    def getSoundMessage(self):
        last_time = time.time()
        self.openStream()
        with RawAudioStream(self.pipe.stdout, 8000) as rawSource:
            result = None
            while self.pipe.stdout.readable():
                # print('Checking silence...')
                # self.recognizer.adjust_for_ambient_noise(rawSource)  # listen for 1 second to calibrate the energy threshold for ambient noise levels
                # print(self.recognizer.energy_threshold)
                print('Say something')
                audioData = self.recognizer.listen(rawSource, None, 30*2) #bug with time long, it in SAMPLE_WIDTH times less than has to be
                self.saveSoundToDisk(audioData)
                print('Recognizing...')
                result = None
                try:
                    # result = self.recognizer.recognize_google(audioData, None, "uk-UA")
                    result = self.recognizer.recognize_google_cloud(audioData, self.google_credentials_json, "en-US").strip()
                except speech_recognition.UnknownValueError:
                    self.sendSoundMessage("Please repeat.")
                    pass
                except:
                    print(sys.exc_info())
                    pass
                past_time = time.time() - last_time
                if result is not None or past_time > 30:
                    print(result)
                    break
        self.pipe.stdout.close()
        self.pipe.terminate()
        return result

    def listenAnswer(self):
        def callback(recognizer, audioData, start_timestamp, end_timestamp):
            answer = None
            try:
                print('Recognizing %s...' % str(end_timestamp - start_timestamp))
                self.saveSoundToDisk(audioData, 'full')
                real_audio_length = len(audioData.frame_data) / (audioData.sample_rate * audioData.sample_width)
                start_time = end_timestamp - real_audio_length
                if mark['start'] is not None and mark['start'] > start_time:
                    audioData = audioData.get_segment((mark['start'] - start_time) * 1000)
                    print("Skip %s seconds" % round(mark['start'] - start_time, 2))
                    self.saveSoundToDisk(audioData, 'cut')
                answer = self.recognizer.recognize_google_cloud(audioData, self.google_credentials_json, "en-US").strip()
            except speech_recognition.UnknownValueError:
                self.sendSoundMessage("Please repeat.")
                pass
            except:
                print(sys.exc_info())
                pass
            if answer is not None:
                self.sendSoundMessage("You said: " + answer)

        def threadedListen():
            with RawAudioStream(self.pipe.stdout, 8000) as rawSource:
                print('Checking silence...')
                self.recognizer.adjust_for_ambient_noise(rawSource)  # listen for 1 second to calibrate the energy threshold for ambient noise levels
                print("energy_threshold = " + str(self.recognizer.energy_threshold))
                while self.state['is'] == 'busy' and time.time() - start_time <= 30:
                    try:  # listen for 1 second, then check again if the stop function has been called
                        start_timestamp = time.time()
                        audioData = self.recognizer.listen(rawSource, None, 30)
                        end_timestamp = time.time()
                    except speech_recognition.WaitTimeoutError:  # listening timed out, just try again
                        pass
                    else:
                        if self.state['is'] == 'busy': callback(self, audioData, start_timestamp, end_timestamp)

                self.state['is'] = 'free'
                self.pipe.stdout.close()
                self.pipe.terminate()
                self.sendSoundMessage("Ok, that is all.")

        start_time = time.time()
        self.openStream()
        listener_thread = threading.Thread(target=threadedListen)
        listener_thread.daemon = True
        listener_thread.start()
        mark = {
            "start": None,
            "end": None
        }
        return mark

    def run(self):
        print("Sound thread: run")

        while True:
            if not self.soundQueue.empty():
                if self.state['is'] == 'busy':
                    time.sleep(1)
                    continue
                sound = self.soundQueue.get()
                if sound['user'] is None:
                    continue
                if self.state['user'] == sound['user'] and self.state['message'] == sound['message'] and time.time() - self.state['timestamp'] < 10:
                    self.state['user'] = None
                    continue
                self.state['is'] = 'busy'
                self.state['user'] = sound['user']
                self.state['message'] = sound['message']
                self.state['timestamp'] = time.time()
                mark = self.listenAnswer()
                self.sendSoundMessage(sound['message'])
                mark['start'] = time.time() + 0.5
            time.sleep(0.1)
