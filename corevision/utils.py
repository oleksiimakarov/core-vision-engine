import cv2
import numpy as np


class Utils:

    @staticmethod
    def find_middle(input_list):
        middle = float(len(input_list)) / 2
        if middle % 2 != 0:
            return input_list[int(middle - .5)]
        else:
            return input_list[int(middle)]

    @staticmethod
    def get_image_focus_value(image):  # returns value in 0 - 255 range
        gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        return np.max(cv2.convertScaleAbs(cv2.Laplacian(gray_image, 3)))
